import React, { Component } from 'react';
import { Col } from 'react-bootstrap';
import { Row } from 'react-bootstrap';

class Novo extends Component {
    render() {
        return (
            <div>
                <br></br>
                <Row className="show-grid text-center">
                    <Col md={12}>
                        <h1>Need an easy way to customize your site?</h1>
                        <h3>React is perfect for that</h3>
                        <p>lorem ipsum bla bla bla</p>
                    </Col>
                    <br></br>
                </Row>
                <br></br><br></br>
                <Row className="show-grid" >
                    <Col md={6}>
                        <h4 className="text-primary">You don't need to have great technical skills.</h4>
                        <p>Lorem ipsum should be here</p>
                    </Col>
                    <Col md={6}>
                        <img height="500" src="https://www.bellmts.ca/file_source/mts/assets/img/wireless_devices/iphones/Apple-iPhone-Xs@2x.png"></img>
                    </Col>

                </Row >
            </div>
        )
    }
}

export default Novo;