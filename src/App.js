import React, { Component } from 'react';
import './App.css';
import Karuzel from './Carousel';
import { Grid } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import Novo from './NewDiv';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Grid fluid>
          <Row className="show-grid">

            <Karuzel />
            <Novo />

          </Row>
        </Grid>
      </div>
    );
  }
}

export default App;
